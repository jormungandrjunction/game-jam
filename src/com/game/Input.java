package com.game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;

public class Input implements MouseListener, MouseMotionListener, KeyListener
{
	private static Map<Integer, Boolean> keyDown = new HashMap<Integer, Boolean>();
	private static Map<Integer, Boolean> keyTyped = new HashMap<Integer, Boolean>();

	private static int mouseX = 0;
	private static int mouseY = 0;
	private static boolean rightButton;
	private static boolean leftButton;

	private static boolean rightButtonTyped;
	private static boolean leftButtonTyped;

	public static boolean isKeyDown(int keyCode)
	{
		Boolean b = keyDown.get(keyCode);
		if(b != null)
			return b;
		else
			return false;
	}
	public static boolean isKeyTyped(int keyCode)
	{
		Boolean b = keyTyped.get(keyCode);
		if(b != null)
			return b;
		else
			return false;
	}
	public static boolean isMouseTyped(int button)
	{
		return ((button == MouseEvent.BUTTON1) ? leftButtonTyped
				: (button == MouseEvent.BUTTON3) ? rightButtonTyped : false);
	}
	public static boolean isMouseDown(int button)
	{
		return ((button == MouseEvent.BUTTON1) ? leftButton
				: (button == MouseEvent.BUTTON3) ? rightButton : false);
	}
	public static double getMouseX()
	{
		return mouseX / GameEngine.getGameScale();
	}
	public static double getMouseY()
	{
		return mouseY / GameEngine.getGameScale();
	}
	public static Point2D getMouse()
	{
		return new Point2D.Double(mouseX, mouseY);
	}
	public void tick()
	{
		keyTyped.clear();
		rightButtonTyped = false;
		leftButtonTyped = false;

	}
	@Override
	public void keyPressed(KeyEvent e)
	{
		Boolean b1 = isKeyDown(e.getKeyCode());
		if(b1 != null && b1 == false)
			keyTyped.put(e.getKeyCode(), true);

		keyDown.put(e.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		keyDown.put(e.getKeyCode(), false);
		keyTyped.put(e.getKeyCode(), false);
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		keyDown.put(e.getKeyCode(), false);
		keyTyped.put(e.getKeyCode(), false);
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
		mouseX = e.getX();
		mouseY = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		mouseX = e.getX();
		mouseY = e.getY();
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		// if(e.getButton() == MouseEvent.BUTTON1)
		// {
		// leftButtonTyped = true;
		// leftButton = true;
		// }
		// if(e.getButton() == MouseEvent.BUTTON3)
		// {
		// rightButtonTyped = true;
		// rightButton = true;
		// }
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		if(e.getButton() == MouseEvent.BUTTON1)
		{
			leftButtonTyped = true;
			leftButton = true;
		}
		if(e.getButton() == MouseEvent.BUTTON3)
		{
			rightButtonTyped = true;
			rightButton = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		if(e.getButton() == MouseEvent.BUTTON1)
		{
			leftButtonTyped = false;
			leftButton = false;
		}
		if(e.getButton() == MouseEvent.BUTTON3)
		{
			leftButtonTyped = false;
			rightButton = false;
		}
	}

}
