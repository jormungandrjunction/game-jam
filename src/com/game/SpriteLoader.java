package com.game;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.IOException;

import javax.imageio.ImageIO;

public class SpriteLoader
{
	public static BufferedImage spriteImage;

	public static float[] scales = { 1f, 1f, 1f, 0.5f };
	public static float[] offsets = new float[4];
	public static RescaleOp rop = new RescaleOp(scales, offsets, null);

	// loads an image from file system and converts it to BufferedImage
	public static BufferedImage load(String name)
	{
		try
		{
			BufferedImage org = ImageIO.read(SpriteLoader.class.getResource(name));
			BufferedImage res = new BufferedImage(org.getWidth(), org.getHeight(),
					BufferedImage.TYPE_INT_ARGB);
			Graphics g = res.getGraphics();
			g.drawImage(org, 0, 0, null, null);
			g.dispose();
			return res;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	public static BufferedImage loadFlipped(String name)
	{
		try
		{
			BufferedImage org = ImageIO.read(SpriteLoader.class.getResource(name));
			BufferedImage res = new BufferedImage(org.getWidth(), org.getHeight(),
					BufferedImage.TYPE_INT_ARGB);
			Graphics g = res.getGraphics();
			g.drawImage(org, org.getWidth(), 0, -org.getWidth(), org.getHeight(), null);
			g.dispose();
			return res;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	// Takes a BufferedImage and covert it to a array sequence
	public static BufferedImage[] splitImage(BufferedImage img, int num, int size)
	{
		BufferedImage bi[] = new BufferedImage[num];
		for(int i = 0; i < num; i++)
		{
			bi[i] = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
			Graphics g = bi[i].getGraphics();
			g.drawImage(img, -i * size, 0, null);
			g.dispose();
		}
		return bi;
	}
	// Takes a BufferedImage and covert it to a 2d array sequence
	public static BufferedImage[][] splitImage(BufferedImage img, int xnum, int ynum, int size)
	{
		BufferedImage bi[][] = new BufferedImage[ynum][xnum];
		for(int i = 0; i < ynum; i++)
		{
			for(int j = 0; j < xnum; j++)
			{
				bi[i][j] = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
				Graphics g = bi[i][j].getGraphics();
				g.drawImage(img, -j * size, -i * size, null);
				g.dispose();
			}

		}
		return bi;
	}
	public static BufferedImage getImage(BufferedImage img, int size, int pos)
	{
		BufferedImage i = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		Graphics g = i.getGraphics();
		g.drawImage(img, -pos * size, 0, null);
		g.dispose();
		return i;
	}
	public static BufferedImage toCompatibleImage(BufferedImage image)
	{
		// obtain the current system graphical settings
		GraphicsConfiguration gfx_config = GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getDefaultScreenDevice().getDefaultConfiguration();
		/*
		 * if image is already compatible and optimized for current system
		 * settings, simply return it
		 */
		if(image.getColorModel().equals(gfx_config.getColorModel()))
			return image;

		// image is not optimized, so create a new image that is
		BufferedImage new_image = gfx_config.createCompatibleImage(image.getWidth(),
				image.getHeight(), image.getTransparency());

		// get the graphics context of the new image to draw the old image on
		Graphics2D g2d = (Graphics2D) new_image.getGraphics();

		// actually draw the image and dispose of context no longer needed
		g2d.drawImage(image, 0, 0, null);
		g2d.dispose();

		// return the new optimized image
		return new_image;
	}
}
