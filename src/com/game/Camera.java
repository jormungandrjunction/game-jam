package com.game;

import com.game.entities.Entity;
import com.game.screens.Screen;

public class Camera
{
	public double x = 0;
	public double y = 0;

	private Screen screen;

	private Entity eMove;
	private double moveX = 0;
	private double moveY = 0;

	private Entity entityFollow;

	public Camera(Screen screen)
	{
		this.screen = screen;
	}
	public void setFollow(Entity e)
	{
		this.entityFollow = e;
	}
	public void tick()
	{
		if(entityFollow != null)
		{
			setCameraPos(entityFollow.x, entityFollow.y);
			move();
		}
		else
		{
			move();
		}
		moveX = 0;
		moveY = 0;
		eMove = null;

	}
	public void setCameraPos(double x, double y)
	{
		double diffX = x - this.x;
		double diffY = y - this.y;

		moveCamera(diffX, diffY);
	}
	private void move()
	{

		this.x += moveX;
		this.y += moveY;
		if(this.x + GameEngine.getGameWidth() > screen.getWidth())
		{
			this.x = screen.getWidth() - GameEngine.getGameWidth();
			moveX = 0;
		}
		if(this.x < 0)
		{
			this.x = 0;
			moveX = 0;
		}

		for(Entity e : GameEngine.getCurrentScreen().getEntities(""))
		{
			if(entityFollow != e)
			{
				e.x -= moveX;
				e.y -= moveY;
			}
		}
		if(eMove != null)
		{
			eMove.x += moveX;
			eMove.y += moveY;
		}
	}
	public void moveCamera(double x, double y)
	{
		moveX = x;
		moveY = y;
	}
	public void moveCamera(Entity e, double x, double y)
	{
		eMove = e;
		moveCamera(x, y);
	}
	public static double getWorldX(double x)
	{
		return GameEngine.getCurrentScreen().getCamera().x + x;
	}
	public static double getWorldY(double y)
	{
		return y;
	}
}
