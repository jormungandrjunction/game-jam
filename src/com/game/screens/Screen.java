package com.game.screens;

import java.awt.Graphics2D;
import java.util.ArrayList;

import com.game.Camera;
import com.game.entities.Entity;

public class Screen
{
	private ArrayList<Entity> entityList = new ArrayList<Entity>();
	private ArrayList<Entity> removeList = new ArrayList<Entity>();
	private ArrayList<Entity> addList = new ArrayList<Entity>();

	protected Camera camera;
	private int width;
	private int height;

	public Screen(int width, int height)
	{
		this.width = width;
		this.height = height;
		camera = new Camera(this);
	}
	public void init()
	{

	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	public void clear()
	{
		entityList.clear();
	}
	public void add(Entity e)
	{
		addList.add(e);
	}
	public void remove(Entity e)
	{
		removeList.add(e);
	}
	public Camera getCamera()
	{
		return camera;
	}
	public void tick()
	{
		addAndRemoveObjects();
		for(Entity e : entityList)
		{
			e.tick();
		}
		camera.tick();
	}
	public ArrayList<Entity> getEntities(String... tags)
	{
		if(tags[0].isEmpty())
		{
			return entityList;
		}
		else
		{
			ArrayList<Entity> list = new ArrayList<>();
			for(Entity e : entityList)
			{
				for(String s : tags)
				{
					if(e.getTag().equals(s))
					{
						list.add(e);
					}
				}

			}
			return list;
		}
	}
	private void addAndRemoveObjects()
	{
		if(removeList.size() > 0)
		{
			entityList.removeAll(removeList);
			removeList.clear();
		}
		if(addList.size() > 0)
		{
			entityList.addAll(addList);
			addList.clear();
		}
	}
	public void render(Graphics2D g, double delta)
	{
		for(Entity e : entityList)
		{
			e.render(g, delta);
		}
	}
}
