package com.game.sound;

import java.io.File;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

public class Sound
{

	private AudioFormat format;
	private AudioInputStream stream;
	private Clip soundClip;
	private DataLine.Info line;
	
	private boolean first = true;

	public Sound(String audioFile)
	{
		try
		{
			//System.out.println();
			stream = AudioSystem.getAudioInputStream(Sound.class.getResource(audioFile));
			format = stream.getFormat();
			line = new DataLine.Info(Clip.class, format);
			soundClip = (Clip) AudioSystem.getLine(line);
			soundClip.open(stream);
		}
		catch(Exception e)
		{
			System.out.println("Unable to find file");
		}
	}
	public void playOnce()
	{
		if(first)
		{
			first = false;
			soundClip.loop(0);
		}
		else
		{
			soundClip.loop(1);
		}
		
//		if(soundClip.isActive() || soundClip.isOpen() || soundClip.isRunning())
//		{
//			soundClip.stop();
//		}
//		soundClip.start();
	}
	public void playSound()
	{
		soundClip.loop(Clip.LOOP_CONTINUOUSLY);
	}

	public void stopSound()
	{
		soundClip.stop();
	}
}
