package com.game;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.Line2D;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import com.game.graphics.Graphic;
import com.game.screens.Screen;

public class GameEngine extends Canvas implements Runnable
{
	private static final long serialVersionUID = 1L;

	// private static GameEngine intance;
	private Input input = new Input();

	private JFrame frame;
	private WindowAdapter wa = new WindowAdapter();
	private int frameWidthPadding;
	private int frameHeightPadding;
	private int windowWidth;
	private int windowHeight;

	private double gameWindowX = 0;
	private double gameWindowY = 0;

	private static int gameWidth;
	private static int gameHeight;
	// private double gameRatio;
	private static double gameScale;

	private boolean running = false;
	private Thread t;

	private static int tickPerSeconds;
	private int skipTicks;
	private int maxFrameSkip = 5;
	private int fps = 0;
	private int tps = 0;

	private long nextGameTick = 0;
	private int loops = 0;
	private double delta = 0;

	private BufferStrategy bs;

	private static Screen currentScreen;

	public static void setCurrentScreen(Screen screen)
	{
		currentScreen = screen;
		currentScreen.init();
	}
	public static Screen getCurrentScreen()
	{
		return currentScreen;
	}
	public static double getGameScale()
	{
		return gameScale;
	}
	public static int getTickRate()
	{
		return tickPerSeconds;
	}

	public GameEngine(int windowWidth, int windowHeight, int gameWidth, int gameHeight,
			double gameScale, int tickPerSeconds, JFrame frame)
	{
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;
		GameEngine.gameWidth = gameWidth;
		GameEngine.gameHeight = gameHeight;
		// this.gameRatio = gameWidth / gameHeight;
		GameEngine.gameScale = gameScale;
		GameEngine.tickPerSeconds = tickPerSeconds;

		this.skipTicks = 1000 / GameEngine.tickPerSeconds;
		this.frame = frame;

		this.setupFrame();

		// this.intance = this;
	}
	// public static GameEngine getInstance()
	// {
	// return intance;
	// }
	private void setupFrame()
	{
		this.setPreferredSize(new Dimension(windowWidth, windowHeight));
		this.setSize(new Dimension(windowWidth, windowHeight));
		this.setSize(windowWidth, windowHeight);

		frame.setLayout(new BorderLayout());
		frame.add(this, BorderLayout.CENTER);

		frame.setSize(new Dimension(this.windowWidth + 6, this.windowHeight + 28));
		frame.setMinimumSize(new Dimension(this.windowWidth, this.windowHeight));
		// frame.pack();

		frameWidthPadding = frame.getWidth() - gameWidth;
		frameHeightPadding = frame.getHeight() - gameHeight;

		frame.setResizable(false);

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		frame.setIgnoreRepaint(true);

		frame.addWindowListener(wa);
		frame.addComponentListener(wa);

		frame.requestFocus();
		frame.requestFocusInWindow();
		this.requestFocus();
		this.requestFocusInWindow();

	}
	public void init()
	{
		this.createBufferStrategy(2);
		bs = this.getBufferStrategy();
	}
	public void start()
	{
		System.out.println("start!");
		setupInput();
		running = true;
		t = new Thread(this);
		t.start();
	}
	public void stop()
	{
		System.out.println("stopping!");
		running = false;
		frame.dispose();
	}
	private void setupInput()
	{
		this.addKeyListener(input);
		this.addMouseListener(input);
		this.addMouseMotionListener(input);
	}
	public static int getGameWidth()
	{
		return gameWidth;
	}
	public static int getGameHeight()
	{
		return gameHeight;
	}

	@Override
	public void run()
	{
		int frames = 0;
		int ticks = 0;
		long lastTimer = System.currentTimeMillis();
		nextGameTick = System.currentTimeMillis();

		while(running)
		{
			try
			{
				Thread.sleep(2);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
			if(Input.isKeyDown(KeyEvent.VK_ESCAPE))
			{
				stop();
				return;
			}

			loops = 0;
			while(System.currentTimeMillis() > nextGameTick && loops < maxFrameSkip)
			{
				tick();
				nextGameTick += skipTicks;
				loops++;
				ticks++;
			}
			delta = (System.currentTimeMillis() + skipTicks - nextGameTick) / (double) skipTicks;
			// System.out.println(delta);
			render(delta);

			frames++;
			if(System.currentTimeMillis() - lastTimer > 1000)
			{
				lastTimer += 1000;
				// System.out.println("Fps:" + frames + "  Ticks:" + ticks);
				fps = frames;
				tps = ticks;
				frames = 0;
				ticks = 0;
			}
		}
	}
	private void render(double delta)
	{
		Graphics2D g = null;
		try
		{
			g = (Graphics2D) bs.getDrawGraphics();
			g.scale(gameScale, gameScale);

			g.clearRect(0, 0, gameWidth, gameHeight);
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, gameWidth, gameHeight);

			// g.translate(gameWindowX, gameWindowY);

			if(currentScreen != null)
			{
				currentScreen.render(g, delta);
			}
			if(Config.DEBUG && currentScreen != null)
			{
				drawDebugInfo(g);
			}
			if(Graphic.lines.size() > 0)
			{
				for(Line2D.Double l : Graphic.lines)
				{
					// g.drawLine((int)l.x1, (int)l.y1, (int)l.x2, (int)l.y2);
				}
				Graphic.lines.clear();
			}
		}
		catch(IllegalStateException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(g != null)
			{
				g.dispose();
			}
		}
		try
		{
			if(bs != null)
				bs.show();
		}
		catch(IllegalStateException e)
		{
			e.printStackTrace();
		}

		Toolkit.getDefaultToolkit().sync();

	}
	private void drawDebugInfo(Graphics2D g)
	{
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, 200, 50);

		double freeRam = Runtime.getRuntime().freeMemory();
		double totalRam = Runtime.getRuntime().totalMemory();
		double ramUsageMB = Math.round((totalRam - freeRam) / 1024.0 / 1024.0 * 100) / 100.0;

		g.setColor(Color.GREEN);
		g.drawString("RAM Usage:" + ramUsageMB + "MB", 5, 15);
		g.drawString("Entities:" + currentScreen.getEntities("").size(), 5, 30);
		g.drawString("FPS:" + fps, 5, 45);
		g.drawString("Ticks:" + tps, 100, 45);
	}
	private void tick()
	{
		if(currentScreen != null)
		{
			currentScreen.tick();
		}
		input.tick();
	}

	private class WindowAdapter implements WindowListener, ComponentListener
	{
		@Override
		public void componentHidden(ComponentEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void componentMoved(ComponentEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void componentResized(ComponentEvent e)
		{

			// double newWidth = e.getComponent().getWidth() -
			// frameWidthPadding;
			// double newHeight = e.getComponent().getHeight() -
			// frameHeightPadding;
			//
			// double newWRatio = newWidth / gameWidth;
			// double newHRatio = newHeight / gameHeight;
			//
			// gameScale = Math.min(newWRatio, newHRatio);
			//
			// gameWindowX = (newWidth - (gameWidth * gameScale)) / 2;
			// gameWindowY = (newHeight - (gameHeight * gameScale)) / 2;

			// System.out.println(gameWindowX);

		}

		@Override
		public void componentShown(ComponentEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowActivated(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowClosed(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowClosing(WindowEvent e)
		{
			stop();
		}

		@Override
		public void windowDeactivated(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowDeiconified(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowIconified(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowOpened(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}
	}
}
