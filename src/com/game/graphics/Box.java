package com.game.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.game.SpriteLoader;

public class Box extends Graphic
{
	public Box(int width, int height, Color color)
	{
		setGraphic(SpriteLoader.toCompatibleImage(createRectangle(width, height, color)));
	}
	private BufferedImage createRectangle(int width, int height, Color color)
	{
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) bi.getGraphics();
		g.setColor(color);
		g.fillRect(0, 0, width, height);
		g.dispose();
		return bi;
	}
}
