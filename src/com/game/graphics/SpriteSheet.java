package com.game.graphics;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.game.SpriteLoader;

public class SpriteSheet
{
	private BufferedImage spriteGraphics;
	private Dimension spriteDimension;
	private BufferedImage[] frames;
	private BufferedImage[] flipFrames;

	public SpriteSheet(String imagePath, int frameSize)
	{
		spriteGraphics = SpriteLoader.load(imagePath);
		spriteDimension = new Dimension(spriteGraphics.getWidth(), spriteGraphics.getHeight());
		// nFrames = (spriteDimension.width / frameSize) *
		// (spriteDimension.height / frameSize);
		frames = splitSprite(spriteDimension, frameSize, false);
		flipFrames = splitSprite(spriteDimension, frameSize, true);
	}
	public int getTotalFrames()
	{
		return frames.length;
	}
	public BufferedImage getFrame(int frame, boolean flip)
	{
		return flip ? flipFrames[frame - 1] : frames[frame - 1];
	}

	private BufferedImage[] splitSprite(Dimension d, int frameSize, boolean flip)
	{
		int numberHorizontalFrames = d.width / frameSize;
		int numberVerticalFrames = d.height / frameSize;
		BufferedImage[] bi = new BufferedImage[numberHorizontalFrames * numberVerticalFrames];
		int index = 0;

		for(int j = 0; j < numberVerticalFrames; j++)
		{
			for(int i = 0; i < numberHorizontalFrames; i++)
			{
				if(flip)
				{
					bi[index] = spriteGraphics.getSubimage(i * frameSize, j * frameSize, frameSize,
							frameSize);

					BufferedImage temp = new BufferedImage(frameSize, frameSize,
							BufferedImage.TYPE_INT_ARGB);
					Graphics2D g = (Graphics2D) temp.getGraphics();
					g.drawImage(bi[index], frameSize, 0, -frameSize, frameSize, null);
					g.dispose();

					bi[index] = SpriteLoader.toCompatibleImage(temp);
				}
				else
				{
					bi[index] = SpriteLoader.toCompatibleImage(spriteGraphics.getSubimage(i
							* frameSize, j * frameSize, frameSize, frameSize));
				}

				index++;
			}
		}

		return bi;
	}
}
