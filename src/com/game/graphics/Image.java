package com.game.graphics;

import com.game.SpriteLoader;

public class Image extends Graphic
{
	public Image(String imagePath)
	{
		this(imagePath, false);
	}
	public Image(String imagePath, boolean flipped)
	{
		if(!flipped)
			setGraphic(SpriteLoader.toCompatibleImage(SpriteLoader.load(imagePath)));
		else
			setGraphic(SpriteLoader.toCompatibleImage(SpriteLoader.loadFlipped(imagePath)));
	}
}
