package com.game.graphics;

import java.util.HashMap;

import com.game.GameEngine;

public class Animation extends Graphic
{
	private SpriteSheet spriteSheet;
	private int frameTick = 0;

	private int currentFrameTick = 0;

	private int currentFrame = 1;

	private HashMap<String, int[]> animations = new HashMap<>();

	private boolean repeat = false;
	private String currentAnimationName = "";
	private int[] currentAnimation;

	private boolean flip = false;

	public Animation(SpriteSheet spriteSheet, int frameTick)
	{
		this.spriteSheet = spriteSheet;
		setGraphic(spriteSheet.getFrame(1, false));
		this.frameTick = GameEngine.getTickRate() - frameTick + 1;
	}
	public void playAnimation(String name, boolean repeat)
	{
		if(!name.equals(currentAnimationName))
		{
			currentFrame = 1;
			currentFrameTick = frameTick;
			currentAnimationName = name;
			currentAnimation = animations.get(name);
		}
		else
		{

		}
		this.repeat = repeat;

	}
	public void setFlip(boolean value)
	{
		this.flip = value;
		setGraphic(spriteSheet.getFrame(currentAnimation[currentFrame - 1], flip));
	}
	public boolean isFlipped()
	{
		return this.flip;
	}
	public void addAnimation(String name, int... frames)
	{
		animations.put(name, frames);
	}
	public String getCurrentAnimation()
	{
		return currentAnimationName;
	}
	public boolean finished()
	{
		if(currentFrame == currentAnimation.length)
			return true;

		return false;
	}

	@Override
	public void tick()
	{
		currentFrameTick++;
		if(currentAnimation != null && currentFrameTick >= frameTick)
		{
			currentFrameTick = 0;
			currentFrame++;

			if(currentFrame > currentAnimation.length)
			{
				if(repeat)
				{
					currentFrame = 1;
				}
				else
				{
					currentFrame = currentAnimation.length;
				}

			}
			setGraphic(spriteSheet.getFrame(currentAnimation[currentFrame - 1], flip));
		}
	}
}
