package com.game.graphics;

public class Invisible extends Graphic
{
	public Invisible()
	{
		setGraphic(null);
	}
}
