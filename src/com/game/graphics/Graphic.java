package com.game.graphics;

import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Graphic
{
	private BufferedImage graphic;

	public static ArrayList<Line2D.Double> lines = new ArrayList<Line2D.Double>();

	protected Graphic()
	{

	}
	public BufferedImage getGraphic()
	{
		return graphic;
	}

	public void setGraphic(BufferedImage g)
	{
		this.graphic = g;
	}
	public void tick()
	{

	}

	public static void drawLine(double x1, double y1, double x2, double y2)
	{
		lines.add(new Line2D.Double(x1, y1, x2, y2));
	}
}
