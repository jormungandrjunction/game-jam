package com.game.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.game.SpriteLoader;

public class Circle extends Graphic
{
	public Circle(int width, int height, Color color)
	{
		setGraphic(SpriteLoader.toCompatibleImage(createCircle(width, height, color)));
	}
	private BufferedImage createCircle(int width, int height, Color color)
	{
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) bi.getGraphics();
		g.setColor(color);
		g.fillOval(0, 0, width, height);
		g.dispose();
		return bi;
	}
}
