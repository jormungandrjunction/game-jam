package com.game.entities;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import com.game.Config;
import com.game.GameEngine;
import com.game.graphics.Graphic;

public class Entity
{
	public double x = 0;
	public double y = 0;

	public int width = 0;
	public int height = 0;

	protected double oldX = 0;
	protected double oldY = 0;

	private Graphic graphic;

	public Rectangle2D.Double hitbox = new Rectangle2D.Double();
	protected double offsetX;
	private double offsetY;

	private String tag = "";

	protected double gravity = 1;
	protected double vy = 0;
	protected double vx = 0;
	protected boolean inAir;
	
	public boolean visible = true;

	public Entity(Graphic g, double x, double y, int width, int height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		this.graphic = g;

		setHitbox(0, 0, width, height);
	}
	public Entity(Graphic g, double x, double y)
	{
		this(g, x, y, 0, 0);
	}
	public Entity(double x, double y, int width, int height)
	{
		this(null, x, y, width, height);
	}
	protected void setTag(String tag)
	{
		this.tag = tag;
	}
	public String getTag()
	{
		return tag;
	}
	public void tick()
	{
		this.oldX = x;
		this.oldY = y;
		updateHitbox();
		graphic.tick();
	}
	protected void setHitbox(double offsetX, double offsetY, double width, double height)
	{
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		hitbox.width = width;
		hitbox.height = height;
	}
	private void updateHitbox()
	{
		hitbox.x = this.x + offsetX;
		hitbox.y = this.y + offsetY;
	}
	public void setGraphics(Graphic graphic)
	{
		this.graphic = graphic;
	}
	public void render(Graphics2D g, double delta)
	{
		AffineTransform af = g.getTransform();
		g.translate(this.getDeltaX(delta), this.getDeltaY(delta));
		
		if(this.visible)
		{
			if(width == 0 || height == 0)
				g.drawImage(graphic.getGraphic(), 0, 0, null);
			else
				g.drawImage(graphic.getGraphic(), 0, 0, width, height, null);
		}
		
		
		g.setTransform(af);
		
		if(Config.HITBOX_DRAW)
		{
			g.setColor(Color.MAGENTA);
			g.draw(hitbox);
		}

		
	}
	public Entity collide(Entity e)
	{
		if(this.hitbox.intersects(e.hitbox))
			return e;

		return null;
	}
	public Entity collideFuture(Entity e, double vx, double vy)
	{
		Rectangle2D.Double hb = new Rectangle2D.Double(e.hitbox.x - vx, e.hitbox.y - vy,
				e.hitbox.width, e.hitbox.height);
		if(this.hitbox.intersects(hb))
			return e;

		return null;
	}
	private double getDeltaX(double delta)
	{
		return oldX + (delta * (x - oldX));
	}
	private double getDeltaY(double delta)
	{
		return oldY + (delta * (y - oldY));
	}
	protected void applyGravity()
	{
		if(inAir)
			this.vy += gravity;

		boolean onGround = false;
		for(Entity e : GameEngine.getCurrentScreen().getEntities("block", "allblock"))
		{
			Entity ee = this.collideFuture(e, 0, vy + 1);
			if(ee != this && ee != null)
			{
				if(vy > 0)
				{
					onGround = true;
					vy = 0;
					inAir = false;
					this.y = e.y - hitbox.height;
				}
				else if(vy < 0)
				{
					vy = 0;
					this.y = e.y + e.hitbox.height;
				}
				else
				{
					onGround = true;
					vy = 0;
					inAir = false;
				}
				continue;
			}
			ee = this.collideFuture(e, vx, 0);
			if(ee != this && ee != null)
			{
				if(vx < 0)
					x = e.x + e.hitbox.width - offsetX;
				else if(vx > 0)
					x = e.x - this.hitbox.width - offsetX;
				vx = 0;
				continue;
			}
		}
		if(!onGround)
		{
			inAir = true;
		}
	}
	public void removeSelf()
	{
		GameEngine.getCurrentScreen().remove(this);
	}
}
