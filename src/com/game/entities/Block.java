package com.game.entities;

import com.game.graphics.Invisible;

public class Block extends Entity
{
	public Block(double x, double y, int width, int height)
	{
		// super(new Box(width,height,Color.BLACK), x, y, width, height);
		super(new Invisible(), x, y, width, height);
		setTag("block");
	}
	public Block(double x, double y, int width, int height, String tag)
	{
		// super(new Box(width,height,Color.BLACK), x, y, width, height);
		super(new Invisible(), x, y, width, height);
		setTag(tag);
	}
}
