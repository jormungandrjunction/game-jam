package com.game.particles;

import com.game.GameEngine;
import com.game.graphics.Graphic;

public class ParticleEmitter
{
	public ParticleEmitter()
	{

	}

	public static void emitt(Graphic particle, double x, double y, double vx, double vy,
			double friction)
	{
		GameEngine.getCurrentScreen().add(new Particle(particle, x, y, vx, vy, friction));
	}
}
