package com.game.particles;

import com.game.entities.Entity;
import com.game.graphics.Graphic;

public class Particle extends Entity
{
	private double friction;

	public Particle(Graphic g, double x, double y, double vx, double vy, double friction)
	{
		super(g, x, y);
		this.friction = friction;
		this.vx = vx;
		this.vy = vy;
	}

	@Override
	public void tick()
	{
		super.tick();

		vx *= friction;
		vy *= friction;

		if(Math.abs(vx) < 0.1)
		{
			vx = 0;
		}
		if(Math.abs(vy) < 0.1)
		{
			vy = 0;
		}

		this.x += vx;
		this.y += vy;

		if(vx == 0 && vy == 0)
		{
			removeSelf();
		}
	}

}
