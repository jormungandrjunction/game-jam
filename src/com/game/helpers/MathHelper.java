package com.game.helpers;

public class MathHelper
{
	public static double getDistance(double x1, double x2, double y1, double y2)
	{
		double xo = x2 - x1;
		double yo = y2 - y1;

		double dist = Math.sqrt(yo * yo + xo * xo);
		return Math.abs(dist);
	}
}
