package com.gamejam.entities;

import java.awt.event.KeyEvent;

import com.game.GameEngine;
import com.game.Input;
import com.game.entities.Entity;
import com.game.graphics.Animation;
import com.game.graphics.SpriteSheet;
import com.game.sound.Sound;

public class Player extends Entity
{
	private Animation a;

	private int health = 3;
	private double jumpHeight = -12;

	private boolean climbing;
	private boolean fire;

	private boolean setCamera;
	
	private Sound shoot = new Sound("/gunshot.wav");

	public Player(double x, double y, int width, int height)
	{
		super(x, y, width, height);
		a = new Animation(new SpriteSheet("/protagspritesheet.png", 32), 21);
		setGraphics(a);
		setHitbox(8, 0, 48, 64);
		setTag("player");

		GameEngine.getCurrentScreen().getCamera().setCameraPos(this.x - 320 + 32, 0);
		setCamera = true;
		a.addAnimation("walk", 1, 2, 3, 2);
		a.addAnimation("stand", 7);
		a.addAnimation("jump", 8);
		a.addAnimation("climb", 10, 11);

		a.addAnimation("fire", 4, 5, 6, 4, 4);

		a.playAnimation("stand", false);
	}
	@Override
	public void tick()
	{
		super.tick();

		if(Input.isKeyDown(KeyEvent.VK_RIGHT) && !fire)
		{
			vx = 5;
			if(!climbing && !inAir)
				a.playAnimation("walk", true);
			a.setFlip(true);

		}
		else if(Input.isKeyDown(KeyEvent.VK_LEFT) && !fire)
		{
			vx = -5;
			if(!climbing && !inAir)
				a.playAnimation("walk", true);
			a.setFlip(false);
		}
		else
		{
			if(!climbing && !inAir && !fire)
				a.playAnimation("stand", false);
			vx = 0;
		}
		if(Input.isKeyDown(KeyEvent.VK_SPACE) && !inAir && !fire)
		{
			inAir = true;
			vy = jumpHeight;
		}
		// TODO if time, fix "limping" walk
		if(Input.isKeyDown(KeyEvent.VK_Z) && !inAir && !climbing)
		{
			fire();
		}

		climbing = false;
		for(Entity e : GameEngine.getCurrentScreen().getEntities("ladder"))
		{
			if(this.collide(e) != null)
			{
				climbing = true;
				break;
			}
		}

		applyGravity();

		if(climbing)
		{
			a.playAnimation("climb", true);
			if(Input.isKeyDown(KeyEvent.VK_UP))
			{
				vy = -5;
			}
			else if(Input.isKeyDown(KeyEvent.VK_DOWN))
			{
				vy = 5;
			}
			else
			{
				vy = 0;
			}
		}

		double worldX = x + GameEngine.getCurrentScreen().getCamera().x;
		// System.out.println(worldX);
		// TODO fix me if got time
		if(!setCamera && worldX > 320 && worldX < GameEngine.getCurrentScreen().getWidth() - 320)
		{
			GameEngine.getCurrentScreen().getCamera().moveCamera(vx, 0);
		}
		else
		{
			setCamera = false;
		}

		checkButtonPress();

		if(inAir && !climbing)
			a.playAnimation("jump", false);

		if(a.getCurrentAnimation().equals("fire") && a.finished())
		{
			fire = false;
		}

		x += vx;
		y += vy;
	}
	private void checkButtonPress()
	{
		if(Input.isKeyTyped(KeyEvent.VK_X))
		{
			for(Entity e : GameEngine.getCurrentScreen().getEntities("button", "ectobutton"))
			{
				if(this.collide(e) != null)
				{
					if(e.getTag().equals("button"))
					{
						Button b = ((Button) e);
						if(b.isPressed())
						{
							b.setPressed(false);
						}
						else
						{
							b.setPressed(true);
						}
						break;
					}
				}
			}
		}
		if(Input.isKeyDown(KeyEvent.VK_X))
		{
			for(Entity e : GameEngine.getCurrentScreen().getEntities("button", "ectobutton"))
			{
				if(this.collide(e) != null)
				{
					if(e.getTag().equals("button"))
					{
						Button b = ((Button) e);
						if(b.isHold())
							b.setPressed(true);

						break;
					}
				}
				
			}
		}
	}

	private void fire()
	{
		double x = this.x;
		double y = this.y;

		if(a.isFlipped())
			x = this.x + 58;

		if(!fire)
		{
			shoot.playOnce();
			a.playAnimation("fire", false);
			fire = true;
			new Bullet(a.isFlipped(), x + 4, y + 20);
		}
	}
	public void takeDamage()
	{
		this.health -= 1;
	}
	public int getHealth()
	{
		return this.health;
	}
}
