package com.gamejam.entities;

import com.game.GameEngine;
import com.game.entities.Entity;
import com.game.graphics.Image;

public class Bullet extends Entity
{

	private boolean isFlipped;

	public Bullet(boolean flipped, double x, double y)
	{
		super(x, y, 4, 4);
		isFlipped = flipped;
		if(isFlipped)
			this.setGraphics(new Image("/Bullet.png"));
		else
			this.setGraphics(new Image("/Bullet.png", true));

		GameEngine.getCurrentScreen().add(this);
		
//		for(Entity e : GameEngine.getCurrentScreen().getEntities("enemy", "block", "door", "player"))
//		{
//			if(this.collide(e) != null)
//			{
//				this.removeSelf();
//				if(e.getTag().equals("enemy"))
//					((Enemy) e).takeDamage();
//				else if (e.getTag().equals("player"))
//					((Player) e).takeDamage();
//				break;
//			}
//		}
	}

	@Override
	public void tick()
	{
		super.tick();
		if(isFlipped)
			this.vx = 15;
		else
			this.vx = -15;
		for(Entity e : GameEngine.getCurrentScreen().getEntities("enemy", "block", "door","player"))
		{
			if(this.collideFuture(e, vx, vy) != null)
			{
				this.removeSelf();
				if(e.getTag().equals("enemy"))
					((Enemy) e).takeDamage();
				else if (e.getTag().equals("player"))
					((Player) e).takeDamage();
				break;
			}
		}
		this.x += this.vx;
	}
}
