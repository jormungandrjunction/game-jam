package com.gamejam.entities;

import com.game.GameEngine;
import com.game.entities.Entity;
import com.game.graphics.Graphic;
import com.game.helpers.MathHelper;

public class ThrowObject extends Entity
{
	boolean thrown;

	private Ghost ghost;

	private double speed = 0;

	public ThrowObject(Graphic g, double x, double y, int width, int height)
	{
		super(g, x, y, width, height);
		setTag("throw");
		thrown = false;
		//vy = 1;
	}
	@Override
	public void tick()
	{
		super.tick();

		if(ghost != null && !thrown)
		{
			double xo = ghost.x - (this.x + width / 2);
			double yo = ghost.y - (this.y + height / 2);

			double angle = Math.atan2(yo, xo);

			double dist = MathHelper.getDistance(xo, 0, yo, 0);

			speed = dist / 2;

			if(speed <= 7)
			{
				speed = 7;
			}

			vx = Math.cos(angle) * speed;
			vy = Math.sin(angle) * speed;

			if(dist < 10)
			{
				vx = 0;
				vy = 0;
			}
		}
		if(vy == 0)
		{
			vx *= 0.8;
		}
		if(Math.abs(vy) < 0.1 && Math.abs(vx) < 0.1)
		{
			vx = 0;
			vy = 0;
			thrown = false;
		}
		if(thrown)
			applyGravity();

		this.x += vx;
		this.y += vy;
	}
	public boolean isThrown()
	{
		return thrown;
	}
	public void setPossessed(Ghost ghost)
	{
		this.ghost = ghost;
	}
	public void throwMe(double vx, double vy)
	{
		thrown = true;
		this.vx = vx;
		this.vy = vy;
	}
	@Override
	public void applyGravity()
	{
		if(inAir)
			this.vy += gravity;

		boolean onGround = false;
		for(Entity e : GameEngine.getCurrentScreen().getEntities("block", "allblock"))
		{
			Entity ee = this.collideFuture(e, 0, vy);
			if(ee != this && ee != null)
			{
				if(vy >= 0)
				{
					onGround = true;
					vy = 0;
					inAir = false;
					this.y = e.y - hitbox.height;
				}
				else if(vy < 0)
				{
					vy = 0;
					this.y = e.y + e.hitbox.height;
				}
				continue;
			}
			ee = this.collideFuture(e, vx, 0);
			if(ee != this && ee != null)
			{
				if(vx < 0)
					x = e.x + e.hitbox.width - offsetX;
				else if(vx > 0)
					x = e.x - this.hitbox.width - offsetX;
				vx = 0;
				continue;
			}
		}
		if(!onGround)
		{
			inAir = true;
		}
	}
}
