package com.gamejam.entities;

import com.game.entities.Entity;
import com.game.graphics.Image;

public class BackgroundImage extends Entity
{

	public BackgroundImage(String image, int width, int height)
	{
		super(0, 0, width, height);
		this.setGraphics(new Image(image));
	}
}
