package com.gamejam.entities;

import java.awt.Color;

import com.game.entities.Entity;
import com.game.graphics.Box;
import com.game.graphics.Image;

public class Door extends Entity
{
	public boolean open = false;
	public double openHeight = 80;
	public double currentOpen = 0;

	public Door(double x, double y)
	{
		super(new Image("/door.png"), x, y, 20, 110);
		openHeight = height;
		setTag("block");
	}
	public Door(double x, double y, boolean ghostblock)
	{
		super(new Image("/ectodoor.png"), x, y, 32, 192);
		openHeight = height;
		setTag("ghostblock");
	}
	public void open()
	{
		this.open = true;
	}
	public void close()
	{
		this.open = false;
	}
	@Override
	public void tick()
	{
		super.tick();

		if(currentOpen < openHeight && open)
		{
			currentOpen += openHeight;
			this.y -= openHeight;
		}
		else if(currentOpen > 0 && !open)
		{
			currentOpen -= openHeight;
			this.y += openHeight;
		}
	}

}
