package com.gamejam.entities;

import java.awt.Color;
import java.awt.event.MouseEvent;

import com.game.GameEngine;
import com.game.Input;
import com.game.entities.Entity;
import com.game.graphics.Circle;
import com.game.graphics.Image;
import com.game.helpers.MathHelper;

public class Ghost extends Entity
{
	private double vx = 0;
	private double vy = 0;
	private double speed = 10;

	private double test = 0;
	private boolean follow = true;

	private double stopX = 0;
	private double stopY = 0;

	private boolean possessing = false;
	private Enemy possessedEnemy = null;

	private ThrowObject to = null;

	public Ghost(double x, double y)
	{
		super(new Image("/ghost.png"), x, y, 32, 32);
	}
	@Override
	public void tick()
	{
		super.tick();

		if(!possessing)
		{
			double xo = Input.getMouseX() - (this.x + width / 2);
			double yo = Input.getMouseY() - (this.y + height / 2);

			double angle = Math.atan2(yo, xo);

			double dist = MathHelper.getDistance(xo, 0, yo, 0);

			speed = dist / 10;

			if(speed <= 7)
			{
				speed = 7;
			}

			vx = Math.cos(angle) * speed;
			vy = Math.sin(angle) * speed;

			if(dist > 5 && follow)
			{
				applyGravity();
				this.x += vx;
				this.y += vy;
				stopX = x;
				stopY = y;
				test = angle;
			}
			else
			{
				follow = false;
//				test += 0.05;
//				double toX = 10 * Math.cos(test) + stopX;
//				double toY = 10 * Math.sin(test) + stopY;
//
//				this.x = toX;
//				this.y = toY;
			}
			if(dist > 20)
			{
				follow = true;
			}
		}
		else
		{
			this.x = possessedEnemy.x;
			this.y = possessedEnemy.y;
		}
		possess();
	}
	private void possess()
	{
		if(possessedEnemy != null)
		{
			if(!possessedEnemy.possessed)
			{
				setPossessing(possessedEnemy, false);
			}
		}
		if(Input.isMouseTyped(MouseEvent.BUTTON3))
		{
			if(possessedEnemy == null)
			{
				for(Entity e : GameEngine.getCurrentScreen().getEntities("enemy"))
				{
					if(this.collide(e) != null)
					{
						setPossessing((Enemy) e, true);
						break;
					}
				}
			}
			else
			{
				setPossessing(possessedEnemy, false);
			}
			if(to == null)
			{
				for(Entity e : GameEngine.getCurrentScreen().getEntities("throw"))
				{
					if(this.collide(e) != null)
					{
						if(!((ThrowObject) e).isThrown())
						{
							to = (ThrowObject) e;
							to.setPossessed(this);
							break;
						}

					}
				}
			}
			else
			{
				to.setPossessed(null);
				if(follow)
					to.throwMe(vx, vy);
				else
					to.throwMe(0, 1);
				to = null;
			}
		}
	}
	private void setPossessing(Enemy e, boolean value)
	{
		if(value)
		{
			if(e.setPossessed(true))
			{
				possessing = true;
				possessedEnemy = e;
			}
		}
		else
		{
			possessing = false;
			if(possessedEnemy != null)
			{
				possessedEnemy.setPossessed(false);
				possessedEnemy.removeSelf();
				possessedEnemy = null;
			}
			

		}
	}
	@Override
	public void applyGravity()
	{
		boolean onGround = false;
		for(Entity e : GameEngine.getCurrentScreen().getEntities("ghostblock", "allblock"))
		{
			Entity ee = this.collideFuture(e, 0, vy);
			if(ee != this && ee != null)
			{
				if(vy >= 0)
				{
					onGround = true;
					vy = 0;
					inAir = false;
					this.y = e.y - hitbox.height;
				}
				else if(vy < 0)
				{
					vy = 0;
					this.y = e.y + e.hitbox.height;
				}
				continue;
			}
			ee = this.collideFuture(e, vx, 0);
			if(ee != this && ee != null)
			{
				if(vx < 0)
					x = e.x + e.hitbox.width - offsetX;
				else if(vx > 0)
					x = e.x - this.hitbox.width - offsetX;
				vx = 0;
				continue;
			}
		}
		if(!onGround)
		{
			inAir = true;
		}
	}

}
