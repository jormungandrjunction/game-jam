package com.gamejam.entities;

import java.awt.Color;

import com.game.entities.Entity;
import com.game.graphics.Box;

public class Button extends Entity
{
	private Door door;
	private boolean hold;
	private boolean pressed;

	public Button(double x, double y, Door door, boolean hold)
	{
		super(new Box(20, 20, Color.ORANGE), x, y, 20, 20);
		this.door = door;
		this.hold = hold;
		setTag("button");
	}
	@Override
	public void tick()
	{
		super.tick();
		if(pressed)
		{
			door.open();
		}
		else
		{
			door.close();
		}
		if(hold)
			this.pressed = false;
	}
	public void setPressed(boolean value)
	{
		this.pressed = value;
	}
	public boolean isPressed()
	{
		return pressed;
	}
	public boolean isHold()
	{
		return hold;
	}

}
