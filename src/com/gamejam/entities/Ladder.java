package com.gamejam.entities;

import com.game.entities.Entity;
import com.game.graphics.Image;

public class Ladder extends Entity
{

	public Ladder(double x, double y)
	{
		super(new Image("/ladder.png"), x, y, 50, 160);
		setTag("ladder");
	}

}
