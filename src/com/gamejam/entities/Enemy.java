package com.gamejam.entities;

import java.awt.event.MouseEvent;

import com.game.GameEngine;
import com.game.Input;
import com.game.entities.Entity;
import com.game.graphics.Animation;
import com.game.graphics.Image;
import com.game.graphics.SpriteSheet;
import com.game.particles.ParticleEmitter;
import com.game.sound.Sound;

public class Enemy extends Entity
{
	private Animation a;
	private int health = 3;

	private double currentDist;
	private double patrolDist;
	private int dir = 1;

	private boolean fire;

	public boolean possessed = false;
	private boolean possessAble;
	
	private boolean sawPlayer = false;
	private int cooldown = 0;

	private Sound shoot = new Sound("/gunshot.wav");

	public Enemy(double x, double y, double patrolDist, boolean possessAble)
	{
		super(x, y, 64, 64);
		this.patrolDist = patrolDist;
		this.currentDist = patrolDist;
		a = new Animation(new SpriteSheet("/enemyspritesheet.png", 32), 20);
		setGraphics(a);
		setTag("enemy");

		this.possessAble = possessAble;

		if(possessAble)
		{
			a.addAnimation("walk", 1, 2);
			a.playAnimation("walk", true);
			a.addAnimation("fire", 3, 3, 3);
		}
		else
		{
			a.addAnimation("walk", 10, 11);
			a.playAnimation("walk", true);
			a.addAnimation("fire", 12, 12, 12);
		}
		a.addAnimation("possessed", 4, 5);
		a.setFlip(true);

		vx = 3;
	}
	public boolean setPossessed(boolean value)
	{
		if(possessAble)
		{
			this.possessed = value;
			vx = 3;
			dir = 1;
			a.setFlip(true);
			return true;
		}
		return false;
	}
	@Override
	public void tick()
	{
		super.tick();

		if(!possessed)
		{
			// System.out.println(currentDist);
			currentDist -= Math.abs(dir * vx);
			a.playAnimation("walk", true);
			if(currentDist <= 0)
			{
				
				currentDist = patrolDist;
				dir *= -1;
				if(dir == 1)
				{
					a.setFlip(true);
				}
				else
				{
					a.setFlip(false);
				}
			}
			vx = 3 * dir;
			
			raytrace();
			cooldown--;
			if(cooldown <= 0)
			{
				cooldown = 25;
				fire = false;
			}
			if(sawPlayer)
			{
				fire();
			}
			
		}
		else
		{
			a.playAnimation("possessed", true);
			if(Input.getMouseX() > this.x + this.width / 2)
			{
				vx = 3;
				a.setFlip(true);
			}
			else if(Input.getMouseX() < this.x + this.width / 2)
			{
				vx = -3;
				a.setFlip(false);
			}

			double xo = Input.getMouseX() - (this.x + this.width / 2);

			double dist = Math.sqrt(xo * xo);
			dist = Math.abs(dist);

			if(dist < this.width / 2)
			{
				vx = 0;
			}
		}
		
		
		
		applyGravity();
		
		

		checkButtonPress();

		checkThrowHit();

		x += vx;
		y += vy;
	}
	private void fire()
	{
		double x = this.x;
		double y = this.y;
		if(a.isFlipped())
			x = this.x + 58;

		if(!fire)
		{
			sawPlayer = false;
			System.out.println("fire");
			shoot.playOnce();
			a.playAnimation("fire", false);
			fire = true;
			new Bullet(a.isFlipped(), x + 4, y + 30);
		}
	}
	private void checkThrowHit()
	{
		for(Entity e : GameEngine.getCurrentScreen().getEntities("throw"))
		{
			Entity ee = e;
			if(this.collide(ee) != null)
			{
				this.removeSelf();
				ee.removeSelf();
			}
		}
	}
	private void raytrace()
	{
		outer:for(int i = 0; i < 200; i++)
		{
			double ri = -i;
			if(a.isFlipped())
			{
				ri *= -1;
			}
			for(Entity e : GameEngine.getCurrentScreen().getEntities(""))
			{
				if(e.hitbox.contains(this.x + ri,
						this.y + 32))
				{
					if(e.getTag().equals("block"))
					{
						// System.out.println("WALL");
						break outer;
					}
					else if(e.getTag().equals("player"))
					{
						// System.out.println("FIRE!");
						sawPlayer = true;
						break;
					}

				}
			}
		}

	}
	public void takeDamage()
	{
		this.health -= 1;
		if(this.health <= 0)
			this.removeSelf();
	}

	private void checkButtonPress()
	{
		if(possessed)
		{
			if(Input.isMouseTyped(MouseEvent.BUTTON1))
			{
				for(Entity e : GameEngine.getCurrentScreen().getEntities("button"))
				{
					if(this.collide(e) != null)
					{
						Button b = ((Button) e);
						if(b.isPressed())
						{
							b.setPressed(false);
						}
						else
						{
							b.setPressed(true);
						}
						break;
					}
				}
			}
			if(Input.isMouseDown(MouseEvent.BUTTON1))
			{
				for(Entity e : GameEngine.getCurrentScreen().getEntities("button"))
				{
					if(this.collide(e) != null)
					{
						Button b = ((Button) e);
						if(b.isHold())
							b.setPressed(true);

						break;
					}
				}
			}
		}
	}
	@Override
	public void removeSelf()
	{
		super.removeSelf();
		this.possessed = false;
		ParticleEmitter.emitt(new Image("/gib1.png"), x + 32, y + 32, 1, -1, 0.9);
		ParticleEmitter.emitt(new Image("/gib2.png"), x + 32, y + 32, -1, -1, 0.9);
		ParticleEmitter.emitt(new Image("/gib3.png"), x + 32, y + 32, 1, 1, 0.9);
		ParticleEmitter.emitt(new Image("/gib4.png"), x + 32, y + 32, -1, 1, 0.9);
	}
}
