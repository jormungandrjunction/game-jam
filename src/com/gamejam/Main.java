package com.gamejam;

import javax.swing.JFrame;

import com.game.GameEngine;
import com.gamejam.screens.Level1;
import com.gamejam.screens.SplashScreen;

public class Main
{
	public static void main(String[] args)
	{
		if(System.getProperty("os.name").startsWith("Win"))
		{
			System.setProperty("sun.java2d.d3d", "False");
		}
		else
		{
			System.setProperty("sun.java2d.opengl", "True");
		}
		System.setProperty("sun.java2d.opengl", "True");

		JFrame frame = new JFrame("Beyond The Cage");
		// frame.setUndecorated(true);
		GameEngine game = new GameEngine(1280, 640, 640, 320, 2, 25, frame);

		game.init();
		game.start();

		GameEngine.setCurrentScreen(new SplashScreen());
	}

}
