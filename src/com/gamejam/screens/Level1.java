package com.gamejam.screens;

import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;

import com.game.Input;
import com.game.entities.Block;
import com.game.graphics.Image;
import com.game.screens.Screen;
import com.game.sound.Sound;
import com.gamejam.entities.BackgroundImage;
import com.gamejam.entities.Boss;
import com.gamejam.entities.Button;
import com.gamejam.entities.Door;
import com.gamejam.entities.Enemy;
import com.gamejam.entities.Ghost;
import com.gamejam.entities.Ladder;
import com.gamejam.entities.Player;
import com.gamejam.entities.ThrowObject;

public class Level1 extends Screen
{

	private Sound music1 = new Sound("/finalmusic.wav");
	private Point2D.Double checkpoint = new Point2D.Double(100, 250);
	private Player p;

	public Level1()
	{
		super(3600, 360);
	}
	@Override
	public void init()
	{
		music1.playSound();
		add(new BackgroundImage("/level1.png", 3600, 320));
		
		
		
		// Roof
		add(new Block(0, 0, 3600, 32, "allblock"));
		// Floor
		add(new Block(0, 320 - 32, 3600, 32, "allblock"));
		// Left wall
		add(new Block(0, 0, 20, 320));
		// Cell roof
		// add(new Block(0, 40, 300, 20));
		// Cell wall
		add(new Block(236, 64, 20, 224 - 110));
		// Cell door
		Door d = new Door(236, 288 - 110);
		add(d);
		// Cell door button
		add(new Button(280, 250, d, false));
		// Ladder
		add(new Ladder(460, 130));
		// Block
		add(new Block(512, 132, 192, 186));
		// Jump to block
		add(new Block(792, 130, 64, 192));

		// Door wall 2
		add(new Block(1132, 64, 20, 116));
		// Door 2
		Door d2 = new Door(1132, 64 + 116);
		add(d2);
		// Button 1 door 2
		add(new Button(1100, 250, d2, false));
		
		//roof wall 2
		add(new Block(1024, 0, 640, 100, "allblock"));
		
		//ectowall 1
		Door e1 = new Door(1272, 96, true);
		add(e1);
		//button to ectowall 1
		add(new Button(1372, 250, e1, false));
		
		// Door wall 3
		add(new Block(1420, 64, 20, 116));
		Door d3 = new Door(1420, 64 + 116);
		add(d3);
		// Button door 3
		add(new Button(1470, 250, d3, false));
		
		
		
		// Ladder 2
		add(new Ladder(1864, 130));
		// Block 2
		add(new Block(1864 + 55, 132, 192, 186));
		// Ladder 3
		add(new Ladder(2112, 130));
		
		
		
		// Ladder 4
		add(new Ladder(2312, 130));
		
		//Lamp
		add(new ThrowObject(new Image("/lamp.png"), 2222, 30, 64, 32));
		
		// Jump to block
		add(new Block(2368, 130, 64, 192));
		// Door wall 4
		add(new Block(2862, 64, 20, 116));
		Door d4 = new Door(2862, 64 + 116);
		add(d4);
		// Button door 
		add(new Button(2802, 250, d4, false));
		
		add(new Ghost(100, 100));
		add(new Enemy(1500, 200, 200, true));
		add(new Enemy(2200, 200, 100, false));
		add(new Enemy(2120, 200, 100, false));
		add(new Enemy(3000, 200, 100, false));
		add(new Enemy(3100, 200, 100, true));
		add(new Enemy(3200, 200, 100, false));
		add(new Enemy(3300, 200, 100, true));
		
		add(new Enemy(300, 200, 100, true));
		
		
		add(new Boss(3300, 320 - 96 - 28));
		
		p = new Player(100, 200, 64, 64);
		add(p);
	}
	
	@Override
	public void tick()
	{
		super.tick();
//		if (p != null)
//		{
//			if (p.x + camera.x >= 400 && p.x  + camera.x < 450)
//				setCheckpoint(new Point2D.Double(450 - camera.x, 250));
//			if(p.getHealth() == 0)
//			{
//				loadCheckpoint();
//			}
//		}
		if(p.getHealth() == 0)
		{
			this.clear();
			init();
		}
		if(Input.isKeyTyped(KeyEvent.VK_R))
		{
			this.clear();
			init();
		}
	}
	
	private void setCheckpoint(Point2D.Double check)
	{
		this.checkpoint = check;
	}
	
	private void loadCheckpoint() {
		p.removeSelf();
		p = new Player(checkpoint.x, checkpoint.y, 63, 64);
		add(p);
	}
}
