package com.gamejam.screens;

import java.awt.event.KeyEvent;

import com.game.GameEngine;
import com.game.Input;
import com.game.screens.Screen;
import com.game.sound.Sound;
import com.gamejam.entities.BackgroundImage;

public class SplashScreen extends Screen
{
	private Sound music = new Sound("/Introsong.wav");
	public SplashScreen()
	{
		super(GameEngine.getGameWidth(), GameEngine.getGameHeight());
		
	}
	@Override
	public void init()
	{
		add(new BackgroundImage("/splash.png",GameEngine.getGameWidth(), GameEngine.getGameHeight()));
		music.playSound();
	}
	@Override
	public void tick()
	{
		super.tick();
		if(Input.isKeyTyped(KeyEvent.VK_ENTER))
		{
			GameEngine.setCurrentScreen(new Level1());
			music.stopSound();
		}
	}
}
